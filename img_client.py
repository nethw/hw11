import socket

import cv2
import numpy as np

mwn = "Client window"
mw = np.zeros(shape=[1024, 1024, 3], dtype=np.uint8)

le_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
le_socket.connect(("127.0.0.1", 23432))

class WHandler:
    def __init__(self):
        self.isDrawing = False
        cv2.namedWindow(winname=mwn)
        cv2.setMouseCallback(mwn,
                             self.mmc)

    def mmc(self, event, x, y, tmp1, tmp2):
        if self.isDrawing:
            le_socket.send(f"{self.x} { self.y} {x} {y}|".encode())
            cv2.line(mw, (self.x, self.y), (x, y), (107, 237, 24), thickness=7)
            print(f"Drawing line from {self.x} { self.y} to {x} {y}.")
        self.x = x
        self.y = y
        if event == cv2.EVENT_LBUTTONDOWN:
            self.isDrawing = True
        elif event == cv2.EVENT_LBUTTONUP:
            self.isDrawing = False

print("Client window appeared on the screan.")
WHandler()
while True:
    cv2.imshow(mwn, mw)
    if cv2.waitKey(20) == 27:
        break
cv2.destroyWindow(mwn)
