import cv2
import socket
import numpy as np

mwn="Server window"
mw = np.zeros(shape=[1024, 1024, 3], dtype=np.uint8)
cv2.namedWindow(winname=mwn)
def drawLine(x1, y1, x2, y2):
    cv2.line(mw, (x1, y1), (x2, y2), (107, 237, 24), thickness=7)

print("Server window appeared on the screan.")
le_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
le_socket.settimeout(100)
le_socket.bind(("127.0.0.1", 23432))
le_socket.listen()
connection, _ =  le_socket.accept()

while True:
    cv2.imshow(mwn, mw)
    if cv2.waitKey(20) == 27:
        break
    for resp in connection.recv(1024).decode().split("|"):
        print("Recieved line cordinates from client: ", resp)
        coords = resp.split()
        if len(coords) < 4:
            continue

        drawLine(*map(int, coords))

connection.close()
cv2.destroyWindow(mwn)
