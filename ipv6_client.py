import socket

my_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
my_socket.connect(("::1", 23432))
print("Sending \"Hello from client!\" to server.")
my_socket.send("Hello from client!".encode())

print("Server response: ", my_socket.recv(1024).decode())