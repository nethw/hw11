import socket

my_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
my_socket.bind(("::1", 23432))
while True:
    my_socket.listen()
    connection, address = my_socket.accept()
    print("Accepted connection from:", address)
    response = connection.recv(1024).decode().upper()
    connection.sendto(response.encode(), address)
    print("Sent", response, "to", address, "\n")